import http.server

class JournalRequestHandler(http.server.BaseHTTPRequestHandler):
	def do_GET(self):
		# Standard template nonsense
		self.send_response(200)	# All ok
		self.end_headers()

		# Our message starts here
		self.wfile.write('Welcome to my web journal!'.encode())

httpd = http.server.HTTPServer(
	('127.0.0.1', 8000),
	JournalRequestHandler
)
httpd.serve_forever()
